define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/spinner/spinner',
	'madjoh_modules/ajax/ajax',
	'madjoh_modules/msgbox/msgbox',
	'madjoh_modules/session/session',
	'madjoh_modules/profile/profile',
	'madjoh_modules/time_comparator/time_comparator'
],
function(require, CustomEvents, Spinner, AJAX, MsgBox, Session, Profile, TimeComparator){
	var Facebook = {
		authResponse : null,

		// CONNECT / DISCONNECT FLOW
			// LOAD
				init : function(settings){
					Facebook.settings = settings;

					if(document.facebookLoading) return document.facebookLoading;

					document.facebookLoading = new Promise(function(resolve, reject){
						if(window.cordova) document.addEventListener('deviceready', resolve, false);
						else{
							var js;
							var fjs = document.getElementsByTagName('script')[0];
							if(!document.getElementById('facebook-jssdk')){
								js = document.createElement('script');
								js.id = 'facebook-jssdk';
								js.addEventListener('load', resolve, false);
								js.src = 'https://connect.facebook.net/en_US/all.js';
								fjs.parentNode.insertBefore(js, fjs);
							}
						}
					});

					document.facebookLoading.then(function(){
						if(!window.cordova){
							FB.init({
								appId      : Facebook.settings.facebookID,
								xfbml      : true,
								version    : 'v2.3'
							});
						}

						Facebook.checkConnection();
					}, function(){console.log('Failed Loading Facebook');});

					return document.facebookLoading;
				},

			// LOGIN
				login : function(){
					Spinner.show();

					return Facebook.connect()
					.then(function(loginResult){return Facebook.getProfile();})
					.then(function(result){
						result.accessToken = Facebook.authResponse.accessToken;
						return Facebook.createMadJoh(result);
					})
					.then(function(response){
						Spinner.hide();
						return response;
					})
					.catch(function(error){
						Spinner.hide();
						return Promise.reject(error);
					});
				},

			// CONNECT
				connect : function(retrying){
					if(document.facebookConnecting && !retrying) return document.facebookConnecting;

					document.facebookConnecting =  new Promise(function(resolve, reject){
						Facebook.init().then(function(){
							if(Facebook.authResponse) return resolve(Facebook.authResponse);

							var scope = ['email', 'user_birthday', 'user_location', 'user_friends'];
							if(window.cordova) 	facebookConnectPlugin.login(scope, resolve, function(result){Facebook.connectionFailed(result, retrying).then(resolve, reject);});
							else 				FB.login(resolve, {scope : scope.join(', ')});
						}, reject);
					});

					document.facebookConnecting.then(
						function(result){console.log(result); Facebook.authResponse = result.authResponse; return result;},
						function(errors){console.log(errors); Facebook.authResponse = result.authResponse; return errors;}
					);

					return document.facebookConnecting;
				},
				connectionFailed : function(result, retrying){
					return new Promise(function(resolve, reject){
						if(result === 'User cancelled.' || result === 'There was a problem logging you in.' || retrying){
							document.facebookConnecting = null;
							return reject(result);
						}

						Facebook.disconnect().then(function(){
							Facebook.connect(true).then(resolve, reject);
						}, reject);
					});
				},

			// DISCONNECT
				disconnect : function(){
					var logoutPromise =  new Promise(function(resolve, reject){
						if(!Facebook.authResponse) return resolve();

						if(window.cordova) 	facebookConnectPlugin.logout(resolve, reject);
						else 				FB.logout(resolve, reject);
					});

					logoutPromise.then(function(result){
						Facebook.authResponse 		= null;
						document.facebookConnecting = null;
						return result;
					});

					return logoutPromise;
				},

			// CHECK CONNECTION
				checkConnection : function(){
					var checkConnectionPromise = new Promise(function(resolve, reject){
						var onSuccess = function(data){Facebook.checkConnectionCallback(data).then(resolve, reject);};
						var onFailure = function(data){resolve({isConnected : false, data : data});};

						Facebook.init().then(function(){
								 if(window.cordova) facebookConnectPlugin.getLoginStatus(onSuccess, onFailure);
							else if(window.FB) 		FB.getLoginStatus(onSuccess);
							else 					return onFailure({error : 'Facebook not found here'});
						}, onFailure);
					});

					return checkConnectionPromise.then(function(result){
						Facebook.authResponse = result.data.authResponse;
						if(result.isConnected) 	console.log('isConnected', 		JSON.stringify(result));
						else 					console.log('isNotConnected', 	JSON.stringify(result));

						return Facebook.authResponse;
					});
				},
				checkConnectionCallback : function(data){
					return new Promise(function(resolve, reject){
						if(data.status === 'connected'){
							if(!Facebook.authResponse && Session.isOpen()) 						Facebook.checkProfile(data).then(resolve, reject);
							else 																return resolve({isConnected : true, 	data : data});
						}else if(data.status === 'unknown' || data.status === 'not_authorized') return resolve({isConnected : false, 	data : data});
					});
				},
				checkProfile : function(data){
					return new Promise(function(resolve, reject){
						Facebook.getProfile().then(function(result){
							Profile.get(function(profile){
								if(result && result.email === profile.mail) return resolve({isConnected : true, 	data : data});
								else 										return resolve({isConnected : false, 	data : data});
							});
						}, reject);
					});
				},

			// GET PROFILE
				getProfile : function(){
					return new Promise(function(resolve, reject){
						Facebook.init().then(function(){
							var url 	= '/me?fields=email,id,first_name,last_name,gender,birthday,location';
							var scope 	= ['email', 'user_birthday', 'user_location'];
							if(window.cordova) 	facebookConnectPlugin.api(url, scope, resolve, reject);
							else 				FB.api(url, {scope : scope.concat(['gender']).join(', ')}, resolve);
						}, reject);
					});
				},

			// CREATE FROM FACEBOOK
				checkBirthdate : function(birthdate){
					birthdate = TimeComparator.parseDate(birthdate, true);
					var age = TimeComparator.getAge(birthdate);
					return age >= 12;
				},
				createMadJoh : function(response){
					return new Promise(function(resolve, reject){
						console.log('Creating MadJoh from data', JSON.stringify(response));

						var birthdate = null;
						if(response.birthday){
							birthdate = response.birthday;
							birthdate = birthdate.split('/');
							birthdate = birthdate[2] + '-' + birthdate[0] + '-' + birthdate[1];

							if(!Facebook.checkBirthdate(birthdate)){
								MsgBox.show('too_young');
								return reject('too_young');
							}
						}

						var gender = response.gender ? (response.gender === 'male' ? 1 : 2 ) : 0;

						var parameters = {
							mail 			: response.email,
							facebook_id 	: response.id,
							firstname 		: response.first_name,
							lastname 		: response.last_name,
							gender 			: gender
						};

						if(birthdate) parameters.birthdate = birthdate;
						if(response.location){
							parameters.location_id 		= response.location.id;
							parameters.location_name 	= response.location.name;
						}
						if(response.accessToken) parameters.accessToken = response.accessToken;

						AJAX.post('/user/login/facebook', {parameters : parameters, noAuth : true, sync : true}).then(function(result){
							Facebook.createCallback(result).then(resolve, reject);
						}, reject);
					});
				},
				createCallback : function(result){
					return new Promise(function(resolve, reject){
						Session.open(result.data);

						if(result.data.first_time_facebook){
							if(!result.data.new_user) MsgBox.show('thanks_facebook_connect', {iconKey : 'JUSTDARE_ICON_ORANGE'});
							Profile.updateCredits(result.data.coins);
							CustomEvents.fireCustomEvent(document, 'FacebookConnected');
						}

						if(result.data.new_user) CustomEvents.fireCustomEvent(document, 'LogEvent', {event : 'Complete Registration', type : 'facebook'});

						return resolve(result);
					});
				},

		// FIND FRIENDS
			// OUTER FRIENDS
				findOuterFriends : function(onSuccess, onError){
					return new Promise(function(resolve, reject){
						Facebook.init().then(function(){
							if(window.cordova) 	facebookConnectPlugin 	.api("/me?fields=invitable_friends", [], 	resolve, reject );
							else 				FB 						.api('/me', {fields : 'invitable_friends'}, resolve 		);
						}, reject);
					});
				},
				findOuterFriendsMore : function(nextURL){
					return AJAX.get(nextURL);
				},

			// INNER FRIENDS
				findInnerFriends : function(){
					return new Promise(function(resolve, reject){
						Facebook.init().then(function(){
							Facebook.connect().then(function(){
								var onSuccess = function(result){Facebook.findInnerFriendsCallback(result).then(resolve, reject);};

								if(window.cordova) facebookConnectPlugin.api("me?fields=friends", [], onSuccess, reject);
								else if(window.FB) FB.api('/me/friends', {}, onSuccess);
							}, reject);
						}, reject);
					});
				},
				findInnerFriendsCallback : function(result){
					var friends = {friends : {}};
					if(result.data) friends.friends.data = result.data;
					else friends = result;
					return Promise.resolve(friends);
				},

			// INVITE
				inviteFriends : function(friends, callback){
					var inner_friend_ids 	= friends[1];
					var outer_friends_ids 	= friends[1].concat(friends[2]).join(',');

					// INNER
					var promise = (inner_friend_ids.length > 0) ? Facebook.inviteInnerFriends(inner_friend_ids) : Promise.resolve({status : 1});
						promise.then(callback);

					// OUTER
					if(Facebook.settings.isFBGame && outer_friends_ids.length > 0){
						Facebook.inviteOuterFriends(outer_friends_ids);
					}
				},
				inviteInnerFriends : function(facebook_ids){
					if(!facebook_ids || facebook_ids.length === 0) return Promise.resolve();
					var parameters = {facebook_ids : JSON.stringify(facebook_ids)};

					return AJAX.post('/friendship/add/facebook', {parameters : parameters});
				},
				inviteOuterFriends : function(outer_friends_ids){
					return new Promise(function(resolve, reject){
						Facebook.init().then(function(){
							Profile.get(function(profile){
								var data = {
									method	: 'apprequests',
									to 		: outer_friends_ids,
									title 	: profile.firstname + ' vous invite sur ' + Facebook.settings.name,
									message : Wording.getText(Facebook.settings.inviteSentence),
								};

								if(window.cordova) 	facebookConnectPlugin.showDialog(data,resolve);
								else if(window.FB) 	FB.ui(data, resolve);
							});
						}, reject);
					});
				},

			// APP INVITES
				appInvite : function(url, picture){
					if(!Session.isOpen()) return;
					var id = Session.getKeys().id;

					if(!url) url 			= Facebook.settings.appInviteURL(id);
					if(!picture) picture 	= Facebook.settings.appInvitePicture;
					Spinner.show();

					Facebook.init().then(function(){
						if(window.cordova){
							facebookConnectPlugin.appInvite(
								{
									url 	: url,
									picture : picture
								},
								function(result){ 	if(result) 	console.log('success', 	result	);},
								function(error){ 	if(error) 	console.log('error', 	error 	);}
							);
						}

						Spinner.hide();
					}, function(){
						MsgBox.show('unavailable');
						Spinner.hide();
					});
				},

			// ADD FRIENDS
				addFriends : function(){
					return new Promise(function(resolve, reject){
						var onSuccess = function(result){
							var friend_ids = [];
							for(var i = 0; i < result.data.length; i++) friend_ids.push(result.data[i].id);
							Facebook.addFriendsPost(friend_ids).then(resolve, reject);
						};

						Facebook.init().then(function(){
							if(window.cordova) 	facebookConnectPlugin.api("me?fields=friends", [], onSuccess, reject);
							else 				FB.api('/me/friends', {}, onSuccess);
						}, reject);
					});
				},
				addFriendsPost : function(friend_ids){
					var parameters = {facebook_ids : JSON.stringify(friend_ids)};

					return AJAX.post('/friendship/add/facebook', {parameters : parameters});
				},

		// SHARE
			share : function(link){
				return new Promise(function(resolve, reject){
					Facebook.init().then(function(){
						var data = {
							method 	: 'share',
							href 	: link ? link : 'https://www.justdareapp.com'
						};

						if(window.cordova) 	facebookConnectPlugin.showDialog(data, resolve, reject);
						else 				FB.ui(data, reject);
					}, reject);
				});
			}
	};

	return Facebook;
});